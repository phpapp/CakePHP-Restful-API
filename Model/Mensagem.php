<?php
class Mensagem extends AppModel {

	public $useTable = 'mensagens';
	
	public $belongsTo = array(
		'Pasta',
		'Remetente' => array(
			'className' => 'Usuario',
			'foreignKey' => 'remetente_id'
		),
		'Destinatario' => array(
			'className' => 'Usuario',
			'foreignKey' => 'destinatario_id'
		),
		'Dono' => array(
			'className' => 'Usuario',
			'foreignKey' => 'dono_id'
		)

	);

}
