<?php
	class Tematica extends AppModel {
		
		public $hasMany = array(
			'ProgramasTematica' => array(
				'foreignKey' => 'tematica_id',
				'className' => 'Api.ProgramasTematica'
			)
		);
		
	}