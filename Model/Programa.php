<?php
	class Programa extends AppModel {
		
		public $belongsTo = array(
			'Usuario' => array(
				'foreignKey' => 'usuario'
			)
		);
		
		public $hasMany = array(
			'ProgramasTematica' => array(
				'foreignKey' => 'programa_id',
				'className' => 'Api.ProgramasTematica'
			)
		);
		
	}