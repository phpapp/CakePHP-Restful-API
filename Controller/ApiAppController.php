<?php
class ApiAppController extends AppController {
	
	public $components = array('Paginator');
	
	private function _populate($pop) {
		return explode(',', $pop);
	}
	
	private function _operations($key) {
		$oper = array(
			'lt' => ' <',
			'gt' => ' >',
			'eq' => '',
			'null' =>' IS NULL',
			'!null' => ' IS NOT NULL'
		);
		return $oper[$key];
	}

	private function _where($where) {
		$queries = explode(',', $where);
		
		$conditions = array();
		
		foreach($queries as $q) {
			$sub = explode('.', $q);
			if ($sub[2]=='null' or $sub[2]=='!null') {
				array_push($conditions, $sub[0].'.'.$sub[1].$this->_operations($sub[2]));
			} else {
				$conditions[$sub[0].'.'.$sub[1].$this->_operations($sub[2])] = $sub[3];
			}
		}

		return $conditions;
	}
	
	private function _order($order) {
		$queries = explode(',', $order);
		
		$orderby = array();
		
		foreach($queries as $q) {
			$sub = explode('.', $q);
			$orderby[$sub[0].'.'.$sub[1]] = $sub[2];
		}
		return $orderby;
	}

	public function index($id = null) {
		$this->layout = 'ajax';
		$query = $this->request->query;

		$limit = (isset($query['limit']))?($query['limit']):(10);
		$populate = (isset($query['populate']))?($this->_populate($query['populate'])):(array());
		$conditions = (isset($query['where']))?($this->_where($query['where'])):(array());
		$order = (isset($query['order']))?($this->_order($query['order'])):(array());

		$this->Paginator->settings = array(
			'conditions' => $conditions,
			'limit' => $limit,
			'order' => $order
		);
		
		if ($this->request->is('get')) {
			$this->{$this->modelClass}->Behaviors->attach('Containable');
			$this->{$this->modelClass}->contain(
				$populate
			);
			
			if (!$id) {
				$data = array(
					'data' => $this->Paginator->paginate($this->modelClass),
					'paginator' => $this->request->paging
				);
				$this->set('data', $data);
			} else {
				$this->set('data', $this->{$this->modelClass}->read(null, $id));
			}
		}
		
		if ($this->request->is('post')) {
			$request = $this->request->input('json_decode');
			$this->{$this->modelClass}->save($request);
			$this->set('data', array('error'=>0, 'message'=>'ok'));
		}
		
		if ($this->request->is('delete')) {
			if ($id) {
				$this->{$this->modelClass}->delete($id);
				$this->set('data', array('error'=>0, 'message'=>'ok'));
			} else {
				$this->set('data', array('error'=>1, 'message'=>'error'));
			}
		}
		
		$this->response->type('json');
		$this->render('Api./json');
	}
	
	public function afterFind($results = array(), $primary) {
		foreach($results as $key => $value) {
			if(isset($results[$key][$this->modelClass]['senha'])) {
				unset($results[$key][$this->modelClass]['senha']);
			}
		}
	}

}
